﻿using System;
using System.Text;
using System.Numerics;
using System.Windows.Forms;
using MaterialSkin.Controls;
using MaterialSkin;
using System.IO;
namespace RSA
{
	public partial class Form1 : MaterialForm
	{
		int E, n, d;
		private string S;
		byte[] array;
		int[] array1;
        public Form1()
        {
            var mat = MaterialSkinManager.Instance;
            mat.AddFormToManage(this);
            mat.Theme = MaterialSkinManager.Themes.LIGHT;
            mat.ColorScheme = new ColorScheme(Primary.Pink200, Primary.Pink200, Primary.Pink200, Accent.Blue100, TextShade.BLACK);
            S = " ";

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
		{
			richTextBox1.Text = S;
		}
		static private bool Testferma(long x)
		{


			//	BigInteger.ModPow()
			Random rand = new Random();
            if (x == 0)
                return false;
			if (x == 2)
				return true;

			for (int i = 0; i < 100; i++)
			{
				long a = rand.Next((int)x);
				if (a <= 2)
					a = a + 2;
				if (BigInteger.ModPow(a, x - 1, x) != 1)
				{
					return false;
				}
			}
			return true;
		}
		private long GCD(long a, long b)
		{
			while (a != 0 && b != 0)
			{
				if (a >= b) a = a % b;
				else b = b % a;
			}
			return a + b; // Одно - ноль
		}
		int Gcd(int a, int b,out int  x, out int  y)
		{
			if (a == 0)
			{
				x = 0; y = 1;
				return b;
			}
			int x1, y1;
			int d = Gcd(b % a, a,out x1, out y1);
			x = y1 - (b / a) * x1;
			y = x1;
			return d;
		}
			private void Encode(out int e, out int n,out int d)
		{
			
            int p, q, w, fi;
			bool c;
			Random rand = new Random();
			do
			{
				p = rand.Next(32000);
				c = Testferma(p);
			}
			while (!c);
			q = p;
			do
			{
				q++;
				c = Testferma(q);
			}
			while (!c);
			n = p * q;
			fi = (p - 1) * (q - 1);
			do
			{
				e = rand.Next(6000);
				Gcd(e, fi, out w, out d);
			}
			while ((GCD(fi, e) != 1)||(w<0));
		d = w;	  
		}

		private void MaterialRaisedButton3_Click(object sender, EventArgs e)
		{
			S = "";
			OpenFileDialog read = new OpenFileDialog
			{
				Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
			};
			if (read.ShowDialog() == DialogResult.OK)
			{
				S = File.ReadAllText(read.FileName, Encoding.GetEncoding(1251));
			}
			richTextBox1.Text = S;
		}

		private void MaterialRaisedButton2_Click(object sender, EventArgs e)
		{

			for (int i = 0; i < array.Length; i++)
			{
				array[i] = (byte)BigInteger.ModPow(array1[i], d, n);
			}

			S = Encoding.GetEncoding(1251).GetString(array);
			richTextBox2.Text = S;
		}

		private void MaterialRaisedButton1_Click(object sender, EventArgs e)
		{
			richTextBox3.Text = "";
			S =	richTextBox1.Text ;
			array = Encoding.GetEncoding(1251).GetBytes(S);

			array1 = new int[array.Length];

			Encode(out E, out n, out d);
			materialSingleLineTextField1.Text = Convert.ToString(E);
			materialSingleLineTextField2.Text = Convert.ToString(d);
			for (int i = 0; i < array.Length; i++)
			{
				array1[i] = (int)BigInteger.ModPow(array[i], E, n);
				richTextBox3.Text += Convert.ToString(array1[i]);
				richTextBox3.Text += " ";
			}
		}
	}
}
